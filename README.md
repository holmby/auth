# Holmby Authentication Library

See projects/auth/README.md for the library readme file.

The library source file is in projects/auth

src/app contains an example application

A php backend can be found at https://gitlab.com/holmby/auth-rest-api/