import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { AppState, Privilege, UserPrivilege } from 'src/app/app.state';

export interface User {
  user: string;
  name: string;
  email: string;
  privileges?: string[];
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // TODO, move to global config
  readonly url = {
      user: '/rest/holmby/v1/user.php',
      changePassword : '/rest/holmby/v1/login.php',
      privileges: '/rest/holmby/v1/privileges.php',
      userPrivileges: '/rest/holmby/v1/userprivileges.php'
  };

  constructor(private http: HttpClient, private store: Store<AppState>) { }

  public loadUsers(): Observable<[User]> {
    return this.http.get<[User]>(this.url.user);
  }

  public createUser(user: User): Observable<[User]> {
    return this.http.post<[User]>(this.url.user, user);
  }

  public deleteUser(user: string): Observable<[User]> {
    return this.http.delete<[User]>(this.url.user, { params: new HttpParams().set('user', user) });
  }

  public changePassword( data: { new_password: string, token: string }): Observable<[User]> {
    return this.http.patch<[User]>(this.url.changePassword, data);
  }

  public loadPrivileges(): Observable<[Privilege]> {
    return this.http.get<[Privilege]>(this.url.privileges);
  }

  public loadUserPrivileges(): Observable<[UserPrivilege]> {
    return this.http.get<[UserPrivilege]>(this.url.userPrivileges);
  }
}
