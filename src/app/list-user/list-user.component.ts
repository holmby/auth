import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, Inject } from '@angular/core';
import { Observable } from 'rxjs';

import { User } from 'src/app/services/user.service';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { AppState, UserPrivilege } from 'src/app/app.state';
import { DeleteUserRequest, UserActionTypes, DeleteUserSuccess, DeleteUserFail } from 'src/app/actions/user.actions';
import { AuthUser, userSelector, LoginDialogComponent } from '@holmby/auth';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

enum LoginStateEnum {
  LoggedOut,
  Authorized,     // logged in hand have 'admin' privilege
  NotAuthorized
}

// --- ConfirmDeleteUserComponent ----------------------------------------------------
@Component({
  selector: 'holmby-confirm-delete-user-dialog',
  templateUrl: './confirm-delete-user-dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmDeleteUserComponent {
  constructor(
      public dialogRef: MatDialogRef<ConfirmDeleteUserComponent>,
      @Inject(MAT_DIALOG_DATA) public user: User
  ) {  }
}

@Component({
  selector: 'holmby-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit, OnDestroy {
  LoginState = LoginStateEnum; // make enum visible in the template
  currentUser: User;

  loginState: LoginStateEnum = LoginStateEnum.LoggedOut;
  private loginUserSubsc;

  users$: Observable<User[]>;
  userPrivileges$: Observable<UserPrivilege[]>;
  private actionSubsc;
  message;
  errorMessage;

  constructor(
      public dialog: MatDialog,
      private store: Store<AppState>,
      private actions$: ActionsSubject
  ) {
    this.loginUserSubsc = this.store.pipe(select(userSelector)).subscribe(user => {
      this.currentUser = user;
      if (!user) {
        this.loginState = LoginStateEnum.LoggedOut;
        this.dialog.open(LoginDialogComponent);
      } else if (0 <= user.privileges.indexOf('admin')) {
        this.loginState = LoginStateEnum.Authorized;
      } else {
        this.loginState = LoginStateEnum.NotAuthorized;
      }
    });
  }

  ngOnInit() {
    this.users$ = this.store.pipe(select('users'));
    this.userPrivileges$ = this.store.pipe(select('userPrivileges'));

    this.actionSubsc = this.actions$.subscribe((action) => {
      if (action.type === UserActionTypes.DELETE_USER_SUCCESS) {
        this.message = 'Användaren ' + (action as DeleteUserSuccess).payload.name + ' togs bort';
      } else if (action.type === UserActionTypes.DELETE_USER_FAIL) {
        this.message = null;
        this.errorMessage = (action as DeleteUserFail).payload;
      }
   });

  }

  ngOnDestroy() {
    if (this.loginUserSubsc) {
      this.loginUserSubsc.unsubscribe();
      this.loginUserSubsc = null;
    }
    if (this.actionSubsc) {
      this.actionSubsc.unsubscribe();
      this.actionSubsc = null;
    }
  }

  onDeleteUser(user: User) {
    if (user.user === this.currentUser.user) {
      alert('Du får inte ta bort dig själv!!!');
    } else {
      const dialogRef = this.dialog.open(ConfirmDeleteUserComponent, {data: user});
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.store.dispatch(new DeleteUserRequest(user.user));
        }
      });
    }
  }

}
