import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'holmby-app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  private readonly serverinfoURL = '/rest/holmby/ServerInfo.php';
  serverinfo;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    return this.http.get(this.serverinfoURL ).subscribe(response => {
                    if (response['success']) {
                      this.serverinfo = response['data'];
                    } else {
                      this.serverinfo = response;
                    }
                  });
  }

}
