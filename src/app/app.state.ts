import { User } from 'src/app/services/user.service';

export interface Privilege {
  privilege: string;
  description: string;
}

export interface UserPrivilege {
  user: string;
  privilege: string;
}

export interface AppState {
  readonly users: User[];
  readonly privileges: Privilege[];
  readonly userPrivileges: UserPrivilege[];
}
