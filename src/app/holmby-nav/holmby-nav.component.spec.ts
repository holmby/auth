
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSidenavModule } from '@angular/material/sidenav';
import { HolmbyNavComponent } from './holmby-nav.component';

describe('HolmbyNavComponent', () => {
  let component: HolmbyNavComponent;
  let fixture: ComponentFixture<HolmbyNavComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatSidenavModule],
      declarations: [HolmbyNavComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HolmbyNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
