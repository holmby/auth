import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { User } from 'src/app/services/user.service';
import { UserPrivilege } from 'src/app/app.state';

@Component({
  selector: 'holmby-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {
  @Input() users: User[];
  @Input() currentUser: User;
  @Input() userPrivileges: UserPrivilege[];
  @Output() deleteUser = new EventEmitter<User>();

  readonly displayedColumns: string[] = ['user', 'name', 'email', 'privilege', 'delete'];

  constructor() { }

  ngOnInit() {
  }

  getPrivileges(user: string) {
    if (!this.userPrivileges) { return ''; }

    let result = '';
    let sep = '';
    this.userPrivileges.forEach(priv => {
      if (priv.user === user) {
        result += sep + priv.privilege; sep = ', ';
      }
    });
    return result;
  }

  onDeleteUser(user) {
    this.deleteUser.emit(user);
  }

}
