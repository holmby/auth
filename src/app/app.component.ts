import { Component } from '@angular/core';

@Component({
  selector: 'holmby-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
