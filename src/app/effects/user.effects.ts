import { Injectable, OnDestroy } from '@angular/core';

import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { Observable, of, defer, from } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';

import { AuthActionTypes, LoginSuccess } from '@holmby/auth';

import { UserActionTypes,
         CreateUserRequest, CreateUserSuccess, CreateUserFail,
         ChangePasswordRequest, ChangePasswordSuccess, ChangePasswordFail,
         DeleteUserRequest, DeleteUserSuccess, DeleteUserFail,
         LoadPrivilegesRequest, LoadPrivilegesSuccess, LoadPrivilegesFail,
         LoadUsersSuccess, LoadUsersFail,
         LoadUserPrivilegesSuccess, LoadUserPrivilegesFail
         } from './../actions/user.actions';
import { User, UserService } from 'src/app/services/user.service';
import { AppState } from 'src/app/app.state';

@Injectable()
export class UserEffects implements OnDestroy {

  // --- Create User ---
  @Effect()
  createUser$ = this.actions$.pipe(
    ofType<CreateUserRequest>(UserActionTypes.CREATE_USER_REQUEST),
    exhaustMap(action =>
      this.userService.createUser(action.payload).pipe(
        map(response => new CreateUserSuccess(response[0]) ),
        catchError(error => of(new CreateUserFail(error)))
      )
    )
  );

  // --- Delete User ---
  @Effect()
  deleteUser$ = this.actions$.pipe(
    ofType<DeleteUserRequest>(UserActionTypes.DELETE_USER_REQUEST),
    exhaustMap(action =>
      this.userService.deleteUser(action.payload).pipe(
        map(response => new DeleteUserSuccess(response[0]) ),
        catchError(error => of(new DeleteUserFail(error)))
      )
    )
  );

  // --- Change Password ---
  @Effect()
  changePassword$ = this.actions$.pipe(
    ofType<ChangePasswordRequest>(UserActionTypes.CHANGE_PASSWORD_REQUEST),
    exhaustMap(action =>
      this.userService.changePassword(action.payload).pipe(
        map(response => new ChangePasswordSuccess() ),
        catchError(error => of(new ChangePasswordFail(error)))
      )
    )
  );

  private actionSubsc;
  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private userService: UserService
  ) {
    // TODO unsubscribe when unloaded
    this.actionSubsc = this.actions$.subscribe((action: Action) => {

      if (action.type === AuthActionTypes.LoginSuccess) {
        const login = action as LoginSuccess;

         // prefetch the list of privileges
        this.userService.loadPrivileges().subscribe(
            response => this.store.dispatch(new LoadPrivilegesSuccess(response)),
            error => alert('Ett fel uppstod när data hämtades från serven: ' + error)
         );

        if (login.payload.privileges.indexOf('admin') > -1) {
          // prefetch user list
          this.userService.loadUsers().subscribe(
             response => this.store.dispatch(new LoadUsersSuccess(response)),
             error => alert('Ett fel uppstod när data hämtades från serven: ' + error)
          );

          // prefetch user privileges
          this.userService.loadUserPrivileges().subscribe(
             response => this.store.dispatch(new LoadUserPrivilegesSuccess(response)),
             error => alert('Ett fel uppstod när data hämtades från serven: ' + error)
          );
        }

      }
    });
  }
  ngOnDestroy() {
    if (this.actionSubsc) {
      this.actionSubsc.unsubscribe();
      this.actionSubsc = null;
    }
  }

}
