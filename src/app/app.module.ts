import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { environment } from '../environments/environment';
import { reducers, metaReducers } from './reducers/reducers';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { HolmbyNavComponent } from './holmby-nav/holmby-nav.component';
import { AboutComponent } from './about/about.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { AuthModule, AuthConfig } from '@holmby/auth';

import { ListUserComponent, ConfirmDeleteUserComponent } from './list-user/list-user.component';
import { UserService } from 'src/app/services/user.service';
import { UserComponent } from './user/user.component';
import { UserTableComponent } from './user-table/user-table.component';
import { UserEffects } from './effects/user.effects';
import { PasswordComponent } from './password/password.component';

const authConfig: AuthConfig = {
  persistent: true,               // store login info in localstore (restore login state when reloading page)

  inactivWarningTime: 25 * 60 * 1000,    // after inactivWarningTime miliseconds of inactivity a warning is shown
  inactivLogoutTime: 30 * 60 * 1000,     // after inactivLogoutTime miliseconds of inactivity the user is logged out

  text: {
    loginUser: 'användare',
    loginPassword: 'lösenord'
  },

  url: {
    login: '/rest/holmby/v1/login.holmby.php',
    forgotPassword: '/rest/holmby/v1/forgotPassword.php',
  }
};

@NgModule({
  declarations: [
    AboutComponent,
    AppComponent,
    ConfirmDeleteUserComponent,
    UserComponent,
    HolmbyNavComponent,
    ListUserComponent,
    PageNotFoundComponent,
    UserTableComponent,
    PasswordComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    LayoutModule,
    ReactiveFormsModule,
    HttpClientModule,

    AuthModule.forRoot(authConfig),

    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatSidenavModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,

    StoreModule.forRoot(reducers, { metaReducers, runtimeChecks: { strictStateImmutability: true, strictActionImmutability: true } }),
    StoreRouterConnectingModule.forRoot( { stateKey: 'router' } ),
    environment.production ? [] : StoreDevtoolsModule.instrument(),
    EffectsModule.forRoot([UserEffects]),
  ],
  entryComponents: [ ConfirmDeleteUserComponent ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
