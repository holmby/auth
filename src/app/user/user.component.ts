import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormArray, Validators, FormGroup, FormControl } from '@angular/forms';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { AppState, Privilege } from 'src/app/app.state';
import { Observable } from 'rxjs';

import { MatDialog } from '@angular/material/dialog';

import { userSelector, AuthUser, LoginDialogComponent } from '@holmby/auth';

import { CreateUserRequest, UserActionTypes, CreateUserSuccess, CreateUserFail } from 'src/app/actions/user.actions';
import { User } from 'src/app/services/user.service';
import { getPrivilegesState } from 'src/app/reducers/reducers';

enum LoginStateEnum {
  LoggedOut,
  Authorized,     // logged in hand have 'admin' privilege
  NotAuthorized
}

@Component({
  selector: 'holmby-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  @ViewChild('form', {static: false}) form;
  userForm = this.fb.group({
    user: ['', Validators.required],
    name: ['', Validators.required],
    email: ['', Validators.required],
    priv: this.fb.group([]),
  });

  allPrivileges: Privilege[];
  formIsSent = false;
  success;
  errorMessage;
  successMessage;

  LoginState = LoginStateEnum; // make enum visible in the template

  loginState: LoginStateEnum = LoginStateEnum.LoggedOut;
  private loginUserSubsc;
  private actionSubsc;

  constructor(private fb: FormBuilder,
              private store: Store<AppState>,
              public dialog: MatDialog,
              private actions$: ActionsSubject) {
    this.loginUserSubsc = this.store.pipe(select(userSelector)).subscribe(user => {
      if (!user) {
        this.loginState = LoginStateEnum.LoggedOut;
        this.dialog.open(LoginDialogComponent);
      } else if (0 <= user.privileges.indexOf('admin')) {
        this.loginState = LoginStateEnum.Authorized;
      } else {
        this.loginState = LoginStateEnum.NotAuthorized;
      }
    });

  this.actionSubsc = actions$.subscribe((action) => {
      if (action.type === UserActionTypes.CREATE_USER_SUCCESS) {
        this.formIsSent = false;
        this.success = true;
        this.errorMessage = null;
        const user: User = (action as CreateUserSuccess).payload;
        this.successMessage = 'Ett konto har skapats. ' + user.name + ' kan logga in med användarnamnet ' + user.user +
                              '. Kontot  är kopplat till ' + user.email;
        this.form.resetForm();
      } else if (action.type === UserActionTypes.CREATE_USER_FAIL) {
        this.formIsSent = false;
        this.success = false;
        this.successMessage = null;
        this.errorMessage = (action as CreateUserFail).payload;
      }
   });
  }

  ngOnInit() {
    // reset process form state
    this.formIsSent = false;
    this.success = null;
    this.errorMessage = null;
    this.successMessage = null;

    // TODO move to inner component to avoid internal subscriptions
    this.store.pipe(select(getPrivilegesState)).subscribe(privileges => {
      this.allPrivileges = privileges;

      const privFormGroup = this.userForm.get('priv') as FormGroup;
      privileges.forEach(priv => privFormGroup.addControl( priv.privilege, new FormControl(false) ));
    });
  }

  ngOnDestroy() {
    if (this.loginUserSubsc) {
      this.loginUserSubsc.unsubscribe();
      this.loginUserSubsc = null;
    }
    if (this.actionSubsc) {
      this.actionSubsc.unsubscribe();
      this.actionSubsc = null;
    }
  }

  onSubmit() {
    if (this.userForm.valid && !this.formIsSent) {
      const value = this.userForm.value['priv'];
      const grantedPrivileges: string[] = Array();
      this.allPrivileges.forEach(priv => {
        if (value[priv.privilege]) { grantedPrivileges.push(priv.privilege); }
      });

      this.formIsSent = true; // wait for previous request to complete before sending a new.
      this.store.dispatch(new CreateUserRequest({ ... this.userForm.value, privileges: grantedPrivileges }));
    } else {
      console.log('invalid form');
    }
  }


}
