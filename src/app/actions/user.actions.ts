import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { User } from 'src/app/services/user.service';
import { Privilege, UserPrivilege } from 'src/app/app.state';

export enum UserActionTypes {
  LOAD_USERS_SUCCESS = 'Load User Success',
  LOAD_USERS_FAIL    = 'Load User Fail',

  CREATE_USER_REQUEST = 'Create User Request',
  CREATE_USER_SUCCESS = 'Create User Success',
  CREATE_USER_FAIL    = 'Create User Fail',

  DELETE_USER_REQUEST = 'Delete User Request',
  DELETE_USER_SUCCESS = 'Delete User Success',
  DELETE_USER_FAIL    = 'Delete User Fail',

  CHANGE_PASSWORD_REQUEST = 'reset password request',
  CHANGE_PASSWORD_SUCCESS = 'reset password success',
  CHANGE_PASSWORD_FAIL = 'reset password fail',

  LOAD_PRIVILEGES_REQUEST = 'load privileges request',
  LOAD_PRIVILEGES_SUCCESS = 'load privileges success',
  LOAD_PRIVILEGES_FAIL = 'load privileges fail',

  LOAD_USER_PRIVILEGES_REQUEST = 'load user privileges request',
  LOAD_USER_PRIVILEGES_SUCCESS = 'load user privileges success',
  LOAD_USER_PRIVILEGES_FAIL = 'load user privileges fail'
}

// --- Load Users ---
export class LoadUsersSuccess implements Action {
  readonly type = UserActionTypes.LOAD_USERS_SUCCESS;
  // payload is the loaded list of users
  constructor(public payload: [User] ) { }
}
export class LoadUsersFail implements Action {
  readonly type = UserActionTypes.LOAD_USERS_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}

// --- Create User ---
export class CreateUserRequest implements Action {
  readonly type = UserActionTypes.CREATE_USER_REQUEST;
  constructor(public payload: User) { }
}
export class CreateUserSuccess implements Action {
  readonly type = UserActionTypes.CREATE_USER_SUCCESS;
  // payload is the created user
  constructor(public payload: User ) { }
}
export class CreateUserFail implements Action {
  readonly type = UserActionTypes.CREATE_USER_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}

// --- Delete User ---
export class DeleteUserRequest implements Action {
  readonly type = UserActionTypes.DELETE_USER_REQUEST;
  constructor(public payload: string) { }
}
export class DeleteUserSuccess implements Action {
  readonly type = UserActionTypes.DELETE_USER_SUCCESS;
  // payload is the deleted user
  constructor(public payload: User ) { }
}
export class DeleteUserFail implements Action {
  readonly type = UserActionTypes.DELETE_USER_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}

// --- Change Password ---
export class ChangePasswordRequest implements Action {
  readonly type = UserActionTypes.CHANGE_PASSWORD_REQUEST;
  constructor(public payload: { new_password: string, token: string }) { }
}
export class ChangePasswordSuccess implements Action {
  readonly type = UserActionTypes.CHANGE_PASSWORD_SUCCESS;
}
export class ChangePasswordFail implements Action {
  readonly type = UserActionTypes.CHANGE_PASSWORD_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}

// --- Load Privileges ---
export class LoadPrivilegesRequest implements Action {
  readonly type = UserActionTypes.LOAD_PRIVILEGES_REQUEST;
}
export class LoadPrivilegesSuccess implements Action {
  readonly type = UserActionTypes.LOAD_PRIVILEGES_SUCCESS;
  // payload is the list of privileges
  constructor(public payload: Privilege[] ) { }
}
export class LoadPrivilegesFail implements Action {
  readonly type = UserActionTypes.LOAD_PRIVILEGES_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}

// --- Load User Privileges ---
export class LoadUserPrivilegesRequest implements Action {
  readonly type = UserActionTypes.LOAD_USER_PRIVILEGES_REQUEST;
}
export class LoadUserPrivilegesSuccess implements Action {
  readonly type = UserActionTypes.LOAD_USER_PRIVILEGES_SUCCESS;
  // payload is the list of privileges
  constructor(public payload: [UserPrivilege] ) { }
}
export class LoadUserPrivilegesFail implements Action {
  readonly type = UserActionTypes.LOAD_USER_PRIVILEGES_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}

export type UserActions = LoadUsersSuccess | LoadUsersFail |
                          CreateUserRequest | CreateUserSuccess | CreateUserFail |
                          DeleteUserRequest | DeleteUserSuccess | DeleteUserFail |
                          ChangePasswordRequest | ChangePasswordSuccess | ChangePasswordFail |
                          LoadPrivilegesRequest | LoadPrivilegesSuccess | LoadPrivilegesFail |
                          LoadUserPrivilegesRequest | LoadUserPrivilegesSuccess | LoadUserPrivilegesFail;
