import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from 'src/app/about/about.component';
import { PageNotFoundComponent } from 'src/app/page-not-found/page-not-found.component';
import { ListUserComponent } from 'src/app/list-user/list-user.component';
import { UserComponent } from 'src/app/user/user.component';
import { PasswordComponent } from 'src/app/password/password.component';

const routes: Routes = [
                          { path: '',
                             redirectTo: '/list-users',
                             pathMatch: 'full'
                           },
                           { path: 'list-users', component: ListUserComponent },
                           { path: 'create-user', component: UserComponent },
                           { path: 'reset-password/:token', component: PasswordComponent },
//                           { path: 'change-password', component: PasswordComponent },
//                           { path: 'about', component: AboutComponent },
                           { path: '**', component: PageNotFoundComponent }
                         ];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
