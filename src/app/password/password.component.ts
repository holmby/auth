import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import { Observable } from 'rxjs';
import { userSelector, AuthUser } from '@holmby/auth';

import { ChangePasswordRequest, ChangePasswordSuccess, ChangePasswordFail, UserActionTypes } from 'src/app/actions/user.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'holmby-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit, OnDestroy {
  passwordForm = this.fb.group({
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required],
  }, {
      validator: PasswordComponent.MatchPassword // your validation method
  });

  loggedinUser$: Observable<AuthUser>;

  validToken = false;
  token;

  formIsSent = false;
  success;
  errorMessage;

  name;
  user;
  email;
  private subsc;

  static MatchPassword(AC: AbstractControl) {
    const password = AC.get('password').value; // to get value in input tag
    const confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
     if (password !== confirmPassword) {
         AC.get('confirmPassword').setErrors( {MatchPassword: true} );
     } else {
         return null;
     }
 }

  constructor(private fb: FormBuilder,
      private route: ActivatedRoute,
      private store: Store<AppState>,
      private actions$: ActionsSubject) {

    this.subsc = actions$.subscribe(action => {
      if (action.type === UserActionTypes.CHANGE_PASSWORD_SUCCESS) {
        this.success = true;
        this.formIsSent = false;
        this.errorMessage = null;
        this.passwordForm.reset();
      } else if (action.type === UserActionTypes.CHANGE_PASSWORD_FAIL) {
        this.success = false;
        this.formIsSent = false;
        this.errorMessage = (action as ChangePasswordFail).payload;
      }
    });
  }

  ngOnDestroy() {
    if (this.subsc) {
      this.subsc.unsubscribe();
      this.subsc = undefined;
    }
  }

  ngOnInit() {
    // reset process form state
    this.formIsSent = false;
    this.success = null;
    this.errorMessage = null;

    this.loggedinUser$ = this.store.pipe(select(userSelector));
    this.token = this.route.snapshot.paramMap.get('token');
    if (this.token) {
      // Reset password process, user identified by a jwt token
      const jwt: {exp: number, sub: string, name: string, email: string} = AuthUser.parseJwt(this.token);
      this.validToken = jwt.exp > Date.now() / 1000;
      if (this.validToken) {
        this.user = jwt.sub;
        this.name = jwt.name;
        this.email = jwt.email;
      }
    } else {
      // TODO, if user is logged in
      this.user = null;
      this.name = null;
      this.email = null;
    }
  }

  onSubmit() {
    if (this.passwordForm.valid && !this.formIsSent) {
      this.formIsSent = true; // wait for previous request to complete before sending a new.
      this.store.dispatch(new ChangePasswordRequest({ new_password: this.passwordForm.value.password, token: this.token }));
    } else {
      console.log('invalid form');
    }
  }

}
