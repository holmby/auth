import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';

import { environment } from '../../environments/environment';
import { AppState, Privilege, UserPrivilege } from 'src/app/app.state';
import { Action } from '@ngrx/store';
import { User } from 'src/app/services/user.service';
import { UserActions, UserActionTypes } from './../actions/user.actions';
import { AuthActions, AuthActionTypes } from '@holmby/auth';

export const metaReducers: MetaReducer<AppState>[] = !environment.production
? []
: [];

const initialUsersState: User[] = [];
const initialPrivilegesState: Privilege[] = [];
const initialUserPrivilegesState: UserPrivilege[] = [];

/**
 * Manage the users: User[] property of the AppState
 * @param state
 * @param action
 */
export function usersReducer(state: User[] = initialUsersState, action: UserActions|AuthActions): User[] {
  switch (action.type) {
  case UserActionTypes.LOAD_USERS_SUCCESS:
    return action.payload;
  case AuthActionTypes.Logout:
    return initialUsersState;
  case UserActionTypes.CREATE_USER_SUCCESS:
    return [...state, action.payload ];
  case UserActionTypes.DELETE_USER_SUCCESS:
    return state.filter(e => e.user !== action.payload.user);
  default:
    return state;
  }
}

export function privilegesReducer(state: Privilege[] = initialPrivilegesState, action: UserActions|AuthActions): Privilege[] {
  switch (action.type) {
  case UserActionTypes.LOAD_PRIVILEGES_SUCCESS:
    return action.payload;
  case AuthActionTypes.Logout:
    return initialPrivilegesState;
  default:
    return state;
  }
}

export function userPrivilegesReducer(state: UserPrivilege[] = initialUserPrivilegesState,
                                      action: UserActions|AuthActions): UserPrivilege[] {
  switch (action.type) {
  case UserActionTypes.LOAD_USER_PRIVILEGES_SUCCESS:
    return action.payload;
  case AuthActionTypes.Logout:
    return initialUserPrivilegesState;
  default:
    return state;
  }
}

export const reducers: ActionReducerMap<AppState> = <ActionReducerMap<AppState, Action>>{
  router: routerReducer,
  users: usersReducer,
  privileges: privilegesReducer,
  userPrivileges: userPrivilegesReducer
};

export const getUserState = createFeatureSelector<User[]>('users');
export const getPrivilegesState = createFeatureSelector<Privilege[]>('privileges');
