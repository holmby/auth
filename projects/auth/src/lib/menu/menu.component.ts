import {
         Component,
         ChangeDetectorRef,
         ChangeDetectionStrategy,
         Inject,
         OnDestroy,
         OnInit
       } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { Store, select, ActionsSubject } from '@ngrx/store';

import { Subscription } from 'rxjs';

import { AuthConfigService } from '../auth.config';
import { AuthUser, LoginData } from '../models/user';
import { AuthStoreState, userSelector } from '../reducers/auth.reducer';
import { Logout, AuthActionTypes, LoginFailure, LoginRequest, ForgotPasswordRequest, ForgotPasswordFailure } from '../actions/auth.actions';
import { HolmbyAuthenticationService } from '../holmby-auth.service';

@Component({
  selector: 'holmby-request-password-dialog',
  templateUrl: './forgot.password.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForgotPasswordComponent implements OnDestroy {
  form = this.fb.group({
    email: ['', Validators.required],
  });

  formIsSent = false;
  success = false;
  public errorText;
  private subsc;

  constructor(
    public dialogRef: MatDialogRef<ForgotPasswordComponent>,
    private store: Store<any>,
    private actionsSubj: ActionsSubject,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder
  ) {
    // reset state when opening to make sure previous submissions are cleared
    // using afterClose() do not work
    dialogRef.afterOpened().subscribe(result => {
      this.formIsSent = false;
      this.success = false;
      this.errorText = null;
      this.cdr.markForCheck();
    });

    // listen to success/failuer of http request
    this.subsc = actionsSubj.subscribe(data => {
      if (data.type === AuthActionTypes.ForgotPasswordFailure) {
        const action: ForgotPasswordFailure = data as ForgotPasswordFailure;
        this.errorText = action.payload;
        this.formIsSent = false;
        this.success = false;
        this.cdr.markForCheck();
      } else if (data.type === AuthActionTypes.ForgotPasswordSuccess) {
        // TODO show confirmation message
        this.errorText = null;
        this.formIsSent = false;
        this.success = true;
        this.cdr.markForCheck();
      }
   });

  }

  ngOnDestroy() {
    if (this.subsc) {
      this.subsc.unsubscribe();
    }
  }

  onSubmit() {
    if (this.form.valid) {
      this.formIsSent = true;
      this.errorText = null;
      this.store.dispatch(new ForgotPasswordRequest(this.form.value.email));
    }
  }
}

// --- LoginDialogComponent ----------------------------------------------------
@Component({
  selector: 'holmby-login-dialog',
  templateUrl: './login.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginDialogComponent implements OnDestroy {
  public data: LoginData = { username: '', password: ''};
  public errorText;
  private subsc;

  constructor(
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    private store: Store<any>,
    private actionsSubj: ActionsSubject,
    private cdr: ChangeDetectorRef,
    public dialog: MatDialog,
    @Inject(AuthConfigService) public config,
  ) {
    // listen to success/failuer of http request
    this.subsc = actionsSubj.subscribe(data => {
      if (data.type === AuthActionTypes.LoginFailure) {
        const action: LoginFailure = data as LoginFailure;
        this.errorText = action.payload.error.message;
        this.cdr.markForCheck();
      } else if (data.type === AuthActionTypes.LoginSuccess) {
        dialogRef.close();
      }
   });

  }

  login($event: LoginData): void {
    this.store.dispatch(new LoginRequest({ ...$event }));
  }

  ngOnDestroy() {
    if (this.subsc) {
      this.subsc.unsubscribe();
    }
  }

  onForgotPassword(event) {
    const dialogRef = this.dialog.open(ForgotPasswordComponent);
    event.stopPropagation();
    return false;
  }
}

// --- MenuComponent -----------------------------------------------------------
@Component({
  selector: 'holmby-auth-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements OnInit, OnDestroy {

  public user: AuthUser;
  private userSubscription: Subscription;

  constructor(
          private service: HolmbyAuthenticationService,
          public dialog: MatDialog,
          private cdr: ChangeDetectorRef,
          public store: Store<any>  ) {
  }

  ngOnInit() {
    this.userSubscription = this.store.pipe(select(userSelector)).subscribe(user => {
      this.user = user;
      this.cdr.markForCheck();
    });
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
      this.userSubscription = undefined;
    }
  }

  logout(): void {
    this.store.dispatch(new Logout());
  }

  openLoginDialog(): void {
    const dialogRef = this.dialog.open(LoginDialogComponent);
  }
}
