import { TestBed, inject } from '@angular/core/testing';

import { HolmbyAuthenticationService } from './holmby-auth.service';

describe('HolmbyAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HolmbyAuthenticationService]
    });
  });

  it('should be created', inject([HolmbyAuthenticationService], (service: HolmbyAuthenticationService) => {
    expect(service).toBeTruthy();
  }));
});
