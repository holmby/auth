import {
  Store,
  createSelector,
  createFeatureSelector,
  ActionReducerMap,
  MemoizedSelector
} from '@ngrx/store';

import { AuthActions, AuthActionTypes } from './../actions/auth.actions';
import { AuthUser } from '../models/user';

export interface AuthStoreState {
  user: AuthUser | null;          // info about the logged in user
}

export const initialUserState: AuthUser = null;
export const initialAuthenticationEnabledState = false;
const localstoreItemName = 'HolmbyAuthUser';

export function userReducer(state: AuthUser = initialUserState, action: AuthActions): AuthUser {
  switch (action.type) {
    case AuthActionTypes.LoginSuccess: {
      const user = action.payload;
      // side effect, store state i percistant store
      localStorage.setItem(localstoreItemName, JSON.stringify(user));
      return user;
    }

    case AuthActionTypes.NewAuthToken: {
      const jwt = action.payload;
      // side effect, store state i percistant store
      let newUser = new AuthUser({user: state.user, name: state.name, email: state.email, jwt: jwt});
      if (state.user === newUser.user) {
        localStorage.setItem(localstoreItemName, JSON.stringify(newUser));
      } else {
        // ignore, the jwt belongs to an earlier user
        // this should not happen since the timer is stopped when a new user log in
        newUser = state;
      }
      return newUser;
    }

    case AuthActionTypes.LoginFailure: {
      return initialUserState;
    }

    case AuthActionTypes.Logout: {
      localStorage.removeItem(localstoreItemName);
      return initialUserState;
    }

    default: {
      return state;
    }
  }
}

export const getUser = (state: AuthStoreState) => state.user;

/**
 * This are all reducers for the @holmby/auth module.
 * It is used to create the feature store: StoreModule.forFeature('auth', reducers)
 */
export const authReducers: ActionReducerMap<any> = {
    user: userReducer
};

export const authStateSelector = createFeatureSelector<AuthStoreState>('auth');

export const userSelector = createSelector( authStateSelector, getUser);

