import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';

import { map, tap } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { LoginData, AuthUser } from './models/user';
import { AuthConfigService } from './auth.config';
import { Store } from '@ngrx/store';
import { LoginSuccess, Logout } from './actions/auth.actions';
import { StoreRouterConnectingModule } from '@ngrx/router-store';

interface LoginHttpSuccessResponse {
  user: string;
  name: string;
  email: string;
  jwt: string;
}

export interface LoginHttpFailResponse {
  developerErrorMessage: string;
  stackTrace: string;
  success: boolean;
  url: string;
  userErrorMessage: string;
}

@Injectable({
  providedIn: 'root',
})
export class HolmbyAuthenticationService {
  private authHeader = undefined;
  private readonly prefix = 'Bearer ';

  public getAuthHeader(): string {
    return this.authHeader;
  }

  constructor(private http: HttpClient,
              private store: Store<any>,
              @Inject(AuthConfigService) private config) {
    // init state, fetch user from window.localStore
    // should be somewhere in effects/reducers, but there is no life time hook there.
    if (config.persistent) {
      const text = localStorage.getItem('HolmbyAuthUser');
      if (text) {
        const user = JSON.parse(text);
        const jwt = AuthUser.parseJwt(user.jwt);
        const now = (new Date()).getTime() / 1000;
        if (jwt.exp >= now) {
          // can not store JWT in interceptor, it might not be created yet. Storing it in the store leads to a race condition.
          this.authHeader = this.prefix + user.jwt;
          this.store.dispatch(new LoginSuccess(user));
        } else {
          localStorage.removeItem('HolmbyAuthUser');
        }
      }
    }
  }

  /**
   * Performe the login, returns an Observable<User> of the user data.
   *
   */
  login({ username, password }: LoginData): Observable<AuthUser> {
    return this.http.post<LoginHttpSuccessResponse>(this.config.url.login, {user: username, password: password} )
    .pipe(map(data => {
      let user = null;
      user = new AuthUser(data);
      if (user.isAuthTokenValid()) {
        this.authHeader = this.prefix + user.jwt;
      } else {
        throwError('Internal error. The server returned an expired jwt token: ' + user.jwt);
      }
      return user;
    }));
  }

  /**
   *  Renew the authentication token for the current user.
   *  Returns an observer which will emit the new jwt.
   */
  renewAuthToken(): Observable<string> {
    return this.http.get<string>(this.config.url.login).pipe(map(data => {
        this.authHeader = this.prefix + data;
        return data;
      }));
  }

  /**
   *  Sends a reset password to email, if it is known.
   *  Returns an undefined value when finished (http response is empty)
   */
  forgotPassword(email: string): Observable<any> {
    const params = new HttpParams().set('email', email);
    return this.http.get(this.config.url.forgotPassword, { params: params } );
  }


}
