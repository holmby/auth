import { InjectionToken } from '@angular/core';

export interface AuthConfig {
  persistent: boolean;

  inactivWarningTime: number;    // after inactivWarningTime miliseconds of inactivity a warning is shown
  inactivLogoutTime: number;     // after inactivLogoutTime miliseconds of inactivity the user is logged out

  text: {
    loginUser: string;
    loginPassword: string;
  };

  url: {
    login: string;
    forgotPassword: string
  };
}

export const AuthConfigService = new InjectionToken<AuthConfig>('AuthConfig');
