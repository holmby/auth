/**
 * Information neede to execute a login
 **/
export interface LoginData {
  username: string;
  password: string;
}

/**
 * User information returned by the server when logging in
 **/
export class AuthUser {
  readonly user: string;
  readonly name: string;
  readonly email: string;
  readonly privileges: { // map: privilege name -> [resource id], i.e. {clubadmin: [12,31], office: []}
    [key: string]: number[];
  };
  payload;  // the decoded jwt
  jwt: string;
  exp: number;

  static parseJwt(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }

  constructor(data: {user: string, name: string, email: string, jwt: string}) {
    this.name = data.name;
    this.email = data.email;
    this.jwt = data.jwt;
    this.payload = AuthUser.parseJwt(data.jwt);
    this.user = this.payload.sub;
    this.privileges = this.payload.privileges;
    this.exp = this.payload.exp;
    // TODO, validate jwt signature
  }

  setJWT(jwt: string) {
    this.jwt = jwt;
    const jwtData: {exp: number, sub: string, privileges: string[]} = AuthUser.parseJwt(jwt);
    this.exp = jwtData.exp;
  }

  isAuthTokenValid(): boolean {
    return this.exp > Date.now() / 1000;
  }
  getTimeout() {
    return this.user;
  }

}
