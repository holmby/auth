import { TestBed } from '@angular/core/testing';

import { HolmbyHttpService } from './holmby-http.service';

describe('HolmbyHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HolmbyHttpService = TestBed.get(HolmbyHttpService);
    expect(service).toBeTruthy();
  });
});
