import { TestBed } from '@angular/core/testing';

import { LogoutTimerService } from './logout-timer.service';

describe('LogoutTimerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogoutTimerService = TestBed.get(LogoutTimerService);
    expect(service).toBeTruthy();
  });
});
