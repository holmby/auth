import { Action } from '@ngrx/store';
import { AuthUser, LoginData } from '../models/user';
import { LoginHttpFailResponse } from '../holmby-auth.service';
import { HttpErrorResponse } from '@angular/common/http';

export enum AuthActionTypes {
  LoginRequest =  '@holmby/auth/LoginRequest',
  LoginSuccess =  '@holmby/auth/LoginSuccess',
  LoginFailure =  '@holmby/auth/LoginFailure',
  Logout =        '@holmby/auth/Logout',
  NewAuthToken =  '@holmby/auth/NewAuthToken',

  ForgotPasswordRequest= '@holmby/auth/ForgotPasswordRequest',
  ForgotPasswordSuccess= '@holmby/auth/ForgotPasswordSuccess',
  ForgotPasswordFailure= '@holmby/auth/ForgotPasswordFailure'
}

/**
 *  Public Actions.
 */
export class LoginRequest implements Action {
  readonly type = AuthActionTypes.LoginRequest;

  constructor(public payload: LoginData) {}
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;

  constructor(public payload: AuthUser ) {}
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure;

  // payload is an error message
  constructor(public payload: HttpErrorResponse) {}
}

export class Logout implements Action {
  readonly type = AuthActionTypes.Logout;
}

export class ForgotPasswordRequest implements Action {
  readonly type = AuthActionTypes.ForgotPasswordRequest;
  // payload is the email address
  constructor(public payload: string ) {}
}

export class ForgotPasswordSuccess implements Action {
  readonly type = AuthActionTypes.ForgotPasswordSuccess;
}

export class ForgotPasswordFailure implements Action {
  readonly type = AuthActionTypes.ForgotPasswordFailure;
  // payload is an error message
  constructor(public payload: string ) {}
}

export class NewAuthToken implements Action {
  readonly type = AuthActionTypes.NewAuthToken;
  // the new jwt token
  constructor(public payload: string ) {}
}

export type AuthActions =
  | LoginRequest
  | LoginSuccess
  | LoginFailure
  | Logout
  | ForgotPasswordRequest
  | ForgotPasswordSuccess
  | ForgotPasswordFailure
  | NewAuthToken
  ;
