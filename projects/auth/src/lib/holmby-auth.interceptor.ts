import { Injectable, Injector, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { Store, ActionsSubject, select } from '@ngrx/store';

import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

import * as fromAuthReducers from './reducers/auth.reducer';
import { HolmbyAuthenticationService } from './holmby-auth.service';

/**
 * AuthenticateInterceptor adds an Authorization http header to all outgoing http requests.
 * The header follows Auth0 syntax
 * The value of the Auth header is: 'Bearer ' + JWT-token
 */

@Injectable()
export class HolmbyAuthenticationInterceptor implements HttpInterceptor, OnDestroy {

  static readonly authHeaderName = 'Auth';

  constructor(private store: Store<any>, private authService: HolmbyAuthenticationService) {
  }

  ngOnDestroy() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // if we have an authetication token, add it to the request
    const authHeader = this.authService.getAuthHeader();
    if (authHeader) {
      req = req.clone({headers: req.headers.set(HolmbyAuthenticationInterceptor.authHeaderName, authHeader)});
    }
    return next.handle(req);
  }

}
