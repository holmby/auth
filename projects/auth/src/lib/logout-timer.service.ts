import { ChangeDetectionStrategy, Component, Inject, Injectable, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { fromEvent, merge, of, timer } from 'rxjs';
import { mapTo, switchMap, takeUntil, take, tap } from 'rxjs/operators';

import { MatDialog } from '@angular/material/dialog';

import { AuthActionTypes, Logout } from './actions/auth.actions';
import { AuthConfigService } from './auth.config';

// --- WarnLogoutDialogComponent ----------------------------------------------------
@Component({
  selector: 'holmby-warn-before-logout-dialog',
  templateUrl: './warn-before-logout.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WarnLogoutDialogComponent {
  constructor() { }
}

// --- LogoutTimerService -----------------------------------------------------------
@Injectable({
  providedIn: 'root'
})
export class LogoutTimerService implements OnDestroy {
  /********************************************************
   *  Logout based on inactivity
   */
  //  set a timeout when loading to handel login based on jwt stored in localstore
  private actionsSubsc;
  private timerSubsc;
  private dialogRef;

  // sources of activity
  private readonly trigger$ = of('init token');  // needed to start the first timer on AuthActionTypes.LoginSuccess
  private readonly keydown$ = fromEvent(document.body, 'keydown').pipe(mapTo('keydown'));
//  private readonly scroll$ = fromEvent(window, 'scroll').pipe(mapTo('scroll'));
  private readonly click$ = fromEvent(window, 'click').pipe(mapTo('click'));
  private readonly activity$ = merge(this.trigger$, this.keydown$, this.click$);

  // a timer that calls doLogout() when expired
  // the timer is restarted on any activty
  private logoutTimer$ = this.activity$.pipe(
    switchMap(dummy => timer(this.config.inactivLogoutTime)),  // max one timer
    take(1), // complete the logoutTimer$
    tap(x => this.doLogout(),
      x => console.error('logout error:' + x),
      () => console.log('logout stream completed')
    )
  );
  // a timer that opens the 'logout warning' dialog when expired
  // the timer is restarted on any activty
  private inactiveTimeout$ = this.activity$.pipe(
    tap(dummy => this.closeWarningDialog()),
    switchMap(dummy => timer(this.config.inactivWarningTime)),
    takeUntil(this.logoutTimer$)
  );

  private openWarningDialog() {
    this.dialogRef = this.dialog.open(WarnLogoutDialogComponent);
  }

  private closeWarningDialog() {
    if (this.dialogRef) {
      this.dialogRef.close();
      this.dialogRef = null;
    }
  }

  private doLogout() {
    this.closeWarningDialog();
    this.store.dispatch(new Logout());
  }

  private stopInactiveTimer() {
    this.closeWarningDialog();
    if (this.timerSubsc) {
      this.timerSubsc.unsubscribe();
    }
    this.timerSubsc = null;
  }

  private startInactiveTimer() {
    this.stopInactiveTimer();  // at moste one timer running
    this.timerSubsc = this.inactiveTimeout$.subscribe(
      (dummy) => this.openWarningDialog(),
      (error: any) => console.error('warning stream error: ' + error),
      () => console.log('warning stream completed')
    );

  }
  constructor(
    public dialog: MatDialog,
    private store: Store<any>,
    private actions$: Actions,
    @Inject(AuthConfigService) private config
  ) {
    this.actionsSubsc = this.actions$.subscribe(action => {
      switch (action.type) {
        case (AuthActionTypes.LoginSuccess):
        this.startInactiveTimer();
        break;
        case (AuthActionTypes.Logout):
        this.stopInactiveTimer();
        break;
      }
    });
  }

  ngOnDestroy() {
    this.stopInactiveTimer();
    if (this.actionsSubsc) {
      this.actionsSubsc.unsubscribe();
      this.actionsSubsc = null;
    }
  }

}
