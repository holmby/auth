import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, timer } from 'rxjs';   // Observable is used indirectly, must be imported!
import { catchError, exhaustMap, map, tap, switchMap } from 'rxjs/operators';

import {
  AuthActionTypes,
  LoginRequest,
  LoginFailure,
  LoginSuccess,
  ForgotPasswordRequest,
  ForgotPasswordSuccess,
  ForgotPasswordFailure,
  Logout,
} from '../actions/auth.actions';
import { HolmbyAuthenticationService } from '../holmby-auth.service';
import { LogoutTimerService } from '../logout-timer.service';
import { RenewJwtService } from '../renew-jwt.service';

@Injectable()
export class AuthEffects {

  // effect sequence:
  // LoginRequest -> authService.login(auth) -> (LoginSuccess | LoginFailure)
  @Effect()
  login$ = this.actions$.pipe(
    ofType<LoginRequest>(AuthActionTypes.LoginRequest),
    switchMap((action) =>
      this.authService.login(action.payload).pipe(
        map(user => new LoginSuccess(user) ),
        catchError(error => of(new LoginFailure(error)))
      )
    )
  );

  // effect sequence:
  // ForgotPasswordRequest -> authService.forgotPassword(email) -> (ForgotPasswordSuccess | ForgotPasswordFailure)
  @Effect()
  forgotPassword$ = this.actions$.pipe(
    ofType<ForgotPasswordRequest>(AuthActionTypes.ForgotPasswordRequest),
    map(action => action.payload),
    switchMap((email: string) =>
      this.authService.forgotPassword(email).pipe(
        map(user => new ForgotPasswordSuccess() ),
        catchError(error => of(new ForgotPasswordFailure(error)))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private authService: HolmbyAuthenticationService,
    private bottstrapping: LogoutTimerService,    // import here to make angular load and start the service
    private bottstrapping2: RenewJwtService       // import here to make angular load and start the service
  ) {
  }
}
