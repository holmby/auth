import { Injectable, OnDestroy } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { NewAuthToken } from './actions/auth.actions';
import { AuthUser } from './models/user';
import { userSelector } from './reducers/auth.reducer';
import { HolmbyAuthenticationService } from './holmby-auth.service';

@Injectable({
  providedIn: 'root'
})
export class RenewJwtService implements OnDestroy {
  private jwtTimer;
  private loginUserSubsc;

  private renewJwt() {
    this.stopJwtTimer();
    this.authService.renewAuthToken().subscribe(
      jwt => {
        this.store.dispatch(new NewAuthToken(jwt));
        const jwtData = AuthUser.parseJwt(jwt);
      },
      error => {
        alert('Ett fel uppstod när inloggningen förnyades. ' +
        'Logga ut och in igen för att återsälla kommunikationen med serven. ' + error);
        console.error('error fetching new jwt: ' + error);
      }
    );
  }

  private startJwtTimer(expire: number) {
    this.stopJwtTimer();
    const wakeupTime = (expire - 60) * 1000 - Date.now();
    this.jwtTimer = setTimeout(() => this.renewJwt(), wakeupTime);
  }

  private stopJwtTimer() {
    if (this.jwtTimer) {
      clearTimeout(this.jwtTimer);
      this.jwtTimer = null;
    }
  }

  constructor(
    private store: Store<any>,
    private authService: HolmbyAuthenticationService,
  ) {
    this.loginUserSubsc = this.store.pipe(select(userSelector)).subscribe(user => {
      if (user) {
        this.startJwtTimer(user.exp);
      } else {
        this.stopJwtTimer();
      }
    });
  }

  ngOnDestroy() {
    this.stopJwtTimer();
    if (this.loginUserSubsc) {
      this.loginUserSubsc.unsubscribe();
      this.loginUserSubsc = null;
    }
  }

}
