import { NgModule, ModuleWithProviders, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';

import { AuthConfig, AuthConfigService } from './auth.config';
import { MenuComponent, LoginDialogComponent, ForgotPasswordComponent } from './menu/menu.component';
import { HolmbyAuthenticationService } from './holmby-auth.service';
import { HolmbyAuthenticationInterceptor } from './holmby-auth.interceptor';
import { authReducers } from './reducers/auth.reducer';
import { AuthEffects } from './effects/auth.effects';
import { LogoutTimerService, WarnLogoutDialogComponent } from './logout-timer.service';
import { RenewJwtService } from './renew-jwt.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MatButtonModule,
    MatChipsModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule
  ],
  declarations: [
    MenuComponent,
    LoginDialogComponent,
    ForgotPasswordComponent,
    WarnLogoutDialogComponent
  ],
  entryComponents: [
    LoginDialogComponent,
    ForgotPasswordComponent,
    WarnLogoutDialogComponent
  ],
  exports: [ MenuComponent ],
  providers: [
              {
                provide: HTTP_INTERCEPTORS,
                useClass: HolmbyAuthenticationInterceptor,
                multi: true,
              },
              HolmbyAuthenticationService,
              LogoutTimerService,
              RenewJwtService
  ]
})
export class AuthModule {
  static forRoot(config: AuthConfig): ModuleWithProviders {
    return {
      ngModule: RootAuthModule,
      providers: [
                  { provide: AuthConfigService, useValue: config },
                ]
    };
  }
}

// --------------

@NgModule({
  imports: [
    AuthModule,
    StoreModule.forFeature('auth', authReducers),
    EffectsModule.forFeature([AuthEffects])
  ],
  exports: [ MenuComponent ],
})
export class RootAuthModule {}
