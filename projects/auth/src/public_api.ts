/*
 * Public API Surface of @holmby/auth
 *
 * Store state:
 *
 * authentication - true if any http request is augmented with an auth-header containing the jwt for the current logged in user.
 *
 * user - data about the logged in user.
 *
 */

/**
 * import the AuthModle in your app.module.ts
 */
export { AuthModule } from './lib/holmby-auth.module';

export { AuthConfig } from './lib/auth.config';
/**
  * User - information about a loged in user.
  * LoginData - data needed to request a login.
  **/
export { AuthUser, LoginData } from './lib/models/user';

/**
 * a selector
 * State -> AuthState.user property
 */
export { userSelector } from './lib/reducers/auth.reducer';

/**
 * a selector
 * State -> AuthState property
 */
export { authStateSelector } from './lib/reducers/auth.reducer';

export { AuthActionTypes,
         AuthActions,
         LoginRequest,
         LoginSuccess,
         LoginFailure,
         Logout
       } from './lib/actions/auth.actions';

export { LoginDialogComponent } from './lib/menu/menu.component';

export { ForgotPasswordComponent } from './lib/menu/menu.component';
