## Holmby Auth

<!-- Badges section here. -->
[![Dependency Status][david-badge]][david-badge-url]
[![devDependency Status][david-dev-badge]][david-dev-badge-url]

[![npm](https://img.shields.io/npm/v/%40holmby/auth.svg)][npm-badge-url]
[![npm](https://img.shields.io/npm/v/%40holmby/auth/next.svg)][npm-badge-url]
[![npm](https://img.shields.io/npm/l/@holmby/auth.svg)][npm-badge-url]
[![npm](https://img.shields.io/npm/dm/@holmby/auth.svg)][npm-badge-url]


[![gitlab forks](https://img.shields.io/gitlab/forks/holmby/auth.svg?style=social&label=Fork)](https://gitlab.com/holmby/auth/fork)
[![gitlab stars](https://img.shields.io/gitlab/stars/holmby/auth.svg?style=social&label=Star)](https://gitlab.com/holmby/auth)

## Prerequisite. You also need to bootstrap StoreModule and EffectsModule.
This module communicates with an authentication server using a REST-api on url /rest/holmby/Login.php.

This module is based on the @ngrx/store architecture, so you need to bootstrap StoreModule and EffectsModule in your app.module.ts.

## Table of Contents

* [Installation](#installation)
* [Usage](#usage)
* [Documentation](#documentation)
* [License](#license)

## Installation

```bash
npm install -save @holmby/auth
```

## Usage

Include the module in your app. You also need to bootstrap StoreModule and EffectsModule.

```typescript
  import { AuthModule, AuthConfig } from '@holmby/auth';

  import { StoreModule } from '@ngrx/store';
  import { StoreRouterConnectingModule } from '@ngrx/router-store';
  import { StoreDevtoolsModule } from '@ngrx/store-devtools';
  import { EffectsModule } from "@ngrx/effects";

  import { environment } from '../environments/environment';

  const authConfig: AuthConfig = {
    url: {
      login: '/rest/holmby/v1/login.php',
    }
  }

  @NgModule({
    imports: [
      AuthModule.forRoot(),

      StoreModule.forRoot({}),
      StoreRouterConnectingModule.forRoot( { stateKey: 'router' } ),
      environment.production ? [] : StoreDevtoolsModule.instrument(),
      EffectsModule.forRoot([]),
    ]
```

In your GUI, add the menu so the user can login:

```html
      <holmby-auth-menu></holmby-auth-menu>
```

The store contains two properties: user and authenticationEnabled.

  * user - information about the loged in user
  * authenticationEnabled - true iff any http request is augmented with a http-auth-header. Wait for this before making any server request that needs authentication.

```typescript
import { User, userSelector, authenticationEnabledSelector } from '@holmby/auth';
export class MyComponent implements OnInit {
  user$: Observable<User>;
  authEnabled$: Observable<boolean>;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.user$ = this.store.pipe(select(userSelector));
    this.authEnabled$ = this.store.pipe(select(authenticationEnabledSelector));
  }
}
```
In your template.

```html
  <h2>AuthModule.user</h2>
  {{ user$ | async | json}}

  <h2>AuthModule.authEnabled</h2>
  {{ authEnabled$ | async | json}}
```

```typescript
export interface User {
  user: string;
  name: string;
  email: string;
  jwt: string;
  privileges: string[];
}
```

When the user is logged in, the jwt is injected in all http request headers (using angulars http inject mechanism).

## Backend
Login is sending a http POST request with body:

```json
  {"user":"pera","password":"hemligt"}
```

The reply should be:

```json
  {
    "user":"pera",
    "name":"Per Andersson",
    "email":"Per@holmby.se",
    "jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJmbHlnc3BvcnQuZXUiLCJzdWIiOiJwZXJhIiwiZXhwIjoxNTM4NTIyODA5fQ.hJrG62y4f804Jshawd0UkFNRlJMB67KCiFwMvAdmu3wJgapNFWAOgfVTXWWvgkRX3HQ_bfMkHRDNOBN7ti-_K1C8VGbB3jZqYMgswRLP_oGoW4VFY_Fo6HqIKxLepufdA7kf6Tw98edk0cCZ4DtLgMnqX09djZhxZp23MLquXj4"
  }
```

or

```json
{
    "message": "Ok\u00e4nd anv\u00e4ndare eller felaktigt l\u00f6senord.",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Ok\u00e4nd anv\u00e4ndare eller felaktigt l\u00f6senord.",
            "file": "/Users/cs-pan/workspaces/shf/www/api/src/Services/ShfLogin.php",
            "line": 44
        }
    ]
}
```


## Documentation

The documentation for the Holmby Auth is located in this repo's [wiki](https://gitlab.com/holmby/auth/wikis/home).

## License

MIT

[travis-badge]: https://travis-ci.org/holmby/auth.svg?branch=master
[travis-badge-url]: https://travis-ci.org/holmby/auth
[david-badge]: https://david-dm.org/holmby/auth.svg
[david-badge-url]: https://david-dm.org/holmby/auth
[david-dev-badge]: https://david-dm.org/holmby/auth/dev-status.svg
[david-dev-badge-url]: https://david-dm.org/holmby/auth?type=dev
[npm-badge]: https://img.shields.io/npm/v/@holmby/auth.svg
[npm-badge-url]: https://www.npmjs.com/package/@holmby/auth
