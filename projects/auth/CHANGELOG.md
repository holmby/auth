## 0.0.10 (2019-12-15)
corrected 0.0.9
maps to number[]
privileges: {[key: string]: number}[]; // map<string: number[]>. privilege name -> [resource id], i.e. {clubadmin: [12,31]}

## 0.0.9 (2019-12-15)
changed type for privileges: readonly
privileges: {[key: string]: number}; // map<string: number[]>. privilege name -> [resource id], i.e. {clubadmin: [12,31]}

## 0.0.8 (2019-12-08)
error message is in the message property of the response.
renewJWT returns the new jwt directly in the body, no json wrapper.

## 0.0.7 (2019-11-09)
Added payload to AuthUser

## 0.0.6 (2019-09-22)
Updated to angular 8.3

## 0.0.2 (2018-10-01)0
Separated store state for User data and autnenticationEnabled (will send http auth header).
Renamed api properties (actions, selectors, classes et.c.).
Added parameter inputs to module (backend url)

## 0.0.1 (2018-09-08)

Initial release. Contains basic functionality for login, logout and automatic sending of the jwt authentication token in all http request headers using the angular http intercept mechanism.
